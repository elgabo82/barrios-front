#!/usr/bin/env bash
yarn
yarn build
docker build --no-cache  -t registry.gitlab.com/wikicarlos/barrios-front/prodfront:latest .
docker push registry.gitlab.com/wikicarlos/barrios-front/prodfront:latest
