export default {
  methods: {
    verificarDigitoVerificador(
      pNumero,
      pIndexVerificador,
      coeficientes,
      pModulo
    ) {
      if (coeficientes == null) {
        return false;
      }
      var tamanio = coeficientes.length;
      var acumulador = 0;
      var residuo = 0;
      var digitoVerifificador = 0;
      var valor;

      try {
        var i;
        for (i = 0; i < tamanio; i++) {
          var digito = parseInt(pNumero.substring(i, i + 1));
          valor = digito * coeficientes[i];
          if (pModulo == 10) {
            if (valor >= 10) {
              valor = valor - 9;
            }
          }
          acumulador = acumulador + valor;
        }

        residuo = acumulador % pModulo;
        digitoVerifificador = residuo == 0 ? 0 : pModulo - residuo;
        return (
          digitoVerifificador ==
          parseInt(pNumero.substring(pIndexVerificador, pIndexVerificador + 1))
        );
      } catch (ex) {
        return false;
      }
    },

    validateRuc(pNumero) {
      var documentType = null;

      if (pNumero.length === 10) {
        documentType = "cedula";
      } else if (pNumero.length === 13) {
        documentType = "ruc";

        // if (pNumero.substring(10, 13) != '001'){
        //   alert('Ingrese su ruc PRINCIPAL / aque que termina en 001')
        // }
      } else {
        return { isValid: false, documentType: null, subType: null };
      }

      // //alert(pNumero)

      var tipo = pNumero.substring(2, 3);
      var modulo11_9 = [4, 3, 2, 7, 6, 5, 4, 3, 2];
      var modulo10 = [2, 1, 2, 1, 2, 1, 2, 1, 2];
      var modulo11_6 = [3, 2, 7, 6, 5, 4, 3, 2];

      var type = null;
      var result = null;
      // debugger
      if (tipo == 9) {
        result = this.verificarDigitoVerificador(pNumero, 9, modulo11_9, 11);
        type = "Juridica";
      } else if (tipo >= 0 && tipo <= 5) {
        result = this.verificarDigitoVerificador(pNumero, 9, modulo10, 10);
        type = "Persona";
      } else if (tipo == 6) {
        result = this.verificarDigitoVerificador(pNumero, 8, modulo11_6, 11);
        type = "Pública";
      } else {
        // //alert('sin resultados')
        return null;
      }

      if (documentType == "ruc") {
        if (pNumero.substring(10, 13) != "001") {
          result = false;
        }
      }

      if (documentType == "cedula" && type != "Persona") {
        result = false;
        documentType = "runcincompleto";
      }

      return { isValid: result, documentType: documentType, subType: type };
    }
  }
};
