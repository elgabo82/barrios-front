import Vue from 'vue'
import App from './App.vue'
import axios from "axios";
import VueAxios from "vue-axios";


Vue.config.productionTip = false
import VueGeolocation from 'vue-browser-geolocation';
Vue.use(VueGeolocation);


if (document.location.protocol == "https:") {
axios.defaults.baseURL = "https://api.enqueayudo.org";

}else{
  axios.defaults.baseURL = "http://localhost:8000";
}
axios.defaults.xsrfCookieName = "csrftoken";
axios.defaults.xsrfHeaderName = "X-CSRFToken";
axios.defaults.timeout = 15000;

Vue.use(VueAxios, axios);

import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAHZa7zzC0gjct8cbU2Dzl9eWJMqo20Yg8',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)

    //// If you want to set the version, you can do so:
    // v: '3.26',
  },
});

new Vue({
  render: h => h(App),
}).$mount('#app')
